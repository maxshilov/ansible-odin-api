#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = '''
---
module: pba_api
short_description:Call PBA API methods I( pba_api)
description:
    - Call PBA API methods from playbooks.
      .
version_added: "1.0"
author:
    - "'Maxim Shilov ( mshilov@gmail.com )'"
notes: []
requirements: []
options:
    name:
        description:
            - API method to execute.
        required: true
        default: null
    user:
        description:
            - User if authentication required.
         required: false
         default: null
    password:
        description:
            - Authentication password
         required: false
         default: null
    host:
        description:
            - PBA API host.
         required: true
         default: null
    arguments:
        description:
            - Dictionary with parameters passed to API method.
         required: true
         default: null
    state:
        description:
            - Desired state of the package.
        required: false
        default: "present"
        choices: ["present"]


'''

EXAMPLES = '''
# call pem.getAccountInfo(12345)
- pba_api: host=10.1.2.3 method=getAccountInfo params=12345

'''


from ansible.module_utils.basic import *
import xmlrpclib
import urllib2


# Transport with  support for xmlrpclib
class Urllib2Transport(xmlrpclib.Transport):
    def __init__(self, opener=None, https=False, use_datetime=0):
        xmlrpclib.Transport.__init__(self, use_datetime)
        self.opener = opener or urllib2.build_opener()
        self.https = https
    
    def request(self, host, handler, request_body, verbose=False):
        proto = ('http', 'https')[bool(self.https)]
        req = urllib2.Request('%s://%s%s' % (proto, host, handler), request_body)
        req.add_header('User-agent', self.user_agent)
        self.verbose = verbose
        return self.parse_response(self.opener.open(req))


class HTTPProxyTransport(Urllib2Transport):
    def __init__(self, proxies, use_datetime=False):
        opener = urllib2.build_opener(urllib2.ProxyHandler(proxies))
        Urllib2Transport.__init__(self, opener, use_datetime)


#transport = HTTPProxyTransport({'http': 'http://10.7.1.13:3128/',})
class POAException(Exception):
    pass


class POA(object):
    def __init__(self, host, user=None, password=None, ssl=False, verbose=False, transport=None, port=8440):
        self.host = host

        protocol = 'http'
        if ssl:
            protocol = 'https'

        if user is not None and user.strip():
            self.__con = xmlrpclib.ServerProxy("%s://%s:%s@%s:%s/RPC2" % (protocol, user, password, host, port),
                                               transport,
                                               allow_none=True)
        else:
            self.__con = xmlrpclib.ServerProxy("%s://%s:%s/RPC2" % (protocol, host, port), transport, allow_none=True)

        self.__con._ServerProxy__verbose = verbose

    def execute(self, method, args):
        m = getattr(self.__con, method)
        if method == "pem.uploadLicense":
            m({"license": xmlrpclib.Binary(open(args['license']).read())})
            return {'result': 0}
        res = m(args)
        res.setdefault('result', 0)
        if res['status'] == -1:
            raise POAException(res['error_message'])
        return res


def main():
    module = AnsibleModule(
        argument_spec=dict(
            name=dict(aliases=['method']),
            state=dict(default='present', choices=['present']),
            user=dict(default=""),
            password=dict(default=""),
            host=dict(required=True),
            arguments=dict(required=True, aliases=['params']),
            server=dict(default='BM')

        ),
        required_one_of=[['name', 'update_cache']],
        supports_check_mode=True)
    
    p = module.params
    user = p["user"]
    password = p["password"]
    host = p["host"]
    method = p["name"]
    arguments = p["arguments"]

    client = POA(host, user, password)
    try:
        res = client.execute(method, arguments)
        module.exit_json(msg=res['result'], changed=True)
    except Exception, e:
        module.fail_json(msg=e.message)


if __name__ == '__main__':
    main()
