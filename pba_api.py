#!/usr/bin/python
# -*- coding: utf-8 -*-

DOCUMENTATION = '''
---
module: pba_api
short_description:Call PBA API methods I( pba_api)
description:
    - Call PBA API methods from playbooks.
      .
version_added: "1.0"
author:
    - "'Maxim Shilov ( mshilov@gmail.com )'"
notes: []
requirements: []
options:
    name:
        description:
            - API method to execute.
        required: true
        default: null
    user:
        description:
            - User if authentication required.
         required: false
         default: null
    password:
        description:
            - Authentication password
         required: false
         default: null
    host:
        description:
            - PBA API host.
         required: true
         default: null
    params:
        description:
            - Dictionary with parameters passed to API method.
         required: true
         default: null
    state:
        description:
            - Desired state of the package.
        required: false
        default: "present"
        choices: ["present"]


'''

EXAMPLES = '''
# call pem.getAccountInfo(12345)
- pba_api: host=10.1.2.3 method=getAccountInfo params=12345

'''


from ansible.module_utils.basic import *
import xmlrpclib
import base64


class PBA(object):
    def __init__(self, host, user=None, password=None, ssl=False, verbose=False, port=5224):
        self.host = host

        protocol = 'http'
        if ssl:
            protocol = 'https'

        if user is not None:
            self.__con = xmlrpclib.ServerProxy("%s://%s:%s@%s:%s/RPC2" % (protocol, user, password, host, port))
        else:
            self.__con = xmlrpclib.ServerProxy("%s://%s:%s/RPC2" % (protocol, host, port))

        self.__con._ServerProxy__verbose = verbose

    def execute(self, method, params=dict(), server='BM'):
        args = {'Params': params, 'Server': server, 'Method': method}
        try:
            response = {
                'status': 0,
                'result': self.__con.Execute(args)['Result'].pop(),
            }
        except xmlrpclib.Fault as e:
            response = {
                'error_message': base64.b64decode(e.faultString).strip(),
                'status': -1,
                'method': method,
                'params': params,
                'server': server,
                'host': self.host,
                'result': None,
            }
        return response


def main():
    module = AnsibleModule(
        argument_spec=dict(
            name=dict(aliases=['method']),
            state=dict(default='present', choices=['present']),

            user=dict(default=""),
            password=dict(default=""),
            host=dict(required=True, ),
            arguments=dict(required=True, aliases=['params']),
            server=dict(default='BM')

        ),
        required_one_of=[['name', 'update_cache']],
        supports_check_mode=True)

    p = module.params

    user = p["user"]
    password = p["password"]
    host = p["host"]
    method = p["name"]
    arguments = p["arguments"]
    server = p["server"]

    client = PBA(host, user, password)
    try:
        res = client.execute(method, arguments, server)
        module.exit_json(msg=res['result'], changed=True)
    except Exception, e:
        module.fail_json(msg=e.message)


if __name__ == '__main__':
    main()